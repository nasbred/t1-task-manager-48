package ru.t1.kharitonova.tm.dto.response.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.kharitonova.tm.dto.model.TaskDTO;

@NoArgsConstructor
public final class TaskRemoveByIndexResponse extends AbstractTaskResponse {

    public TaskRemoveByIndexResponse(@Nullable final TaskDTO taskDTO) {
        super(taskDTO);
    }

}
