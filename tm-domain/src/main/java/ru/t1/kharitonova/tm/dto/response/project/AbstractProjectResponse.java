package ru.t1.kharitonova.tm.dto.response.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.kharitonova.tm.dto.response.AbstractResponse;
import ru.t1.kharitonova.tm.dto.model.ProjectDTO;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractProjectResponse extends AbstractResponse {

    @Nullable
    private ProjectDTO projectDTO;

    public AbstractProjectResponse(@Nullable final ProjectDTO projectDTO) {
        this.projectDTO = projectDTO;
    }

}
