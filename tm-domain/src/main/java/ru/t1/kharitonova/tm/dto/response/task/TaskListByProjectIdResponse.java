package ru.t1.kharitonova.tm.dto.response.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.kharitonova.tm.dto.response.AbstractResponse;
import ru.t1.kharitonova.tm.dto.model.TaskDTO;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public final class TaskListByProjectIdResponse extends AbstractResponse {

    @Nullable
    private List<TaskDTO> taskDTOS;

    public TaskListByProjectIdResponse(@Nullable final List<TaskDTO> taskDTOS) {
        this.taskDTOS = taskDTOS;
    }

}