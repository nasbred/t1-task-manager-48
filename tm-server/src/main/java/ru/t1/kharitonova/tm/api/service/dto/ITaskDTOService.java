package ru.t1.kharitonova.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kharitonova.tm.dto.model.TaskDTO;
import ru.t1.kharitonova.tm.enumerated.Status;
import ru.t1.kharitonova.tm.enumerated.TaskSort;

import java.util.Collection;
import java.util.List;

public interface ITaskDTOService extends IUserOwnedDTOService<TaskDTO> {

    @NotNull
    TaskDTO changeTaskStatusByIndex(@Nullable String userId, @Nullable Integer index, @Nullable Status status);

    @NotNull
    TaskDTO changeTaskStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    @NotNull
    TaskDTO create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @Nullable
    TaskDTO findOneById(@Nullable String userId, @Nullable String id);

    @Nullable
    TaskDTO findOneByIndex(@Nullable String userId, @Nullable Integer index);

    @NotNull
    List<TaskDTO> findAll();

    @NotNull
    List<TaskDTO> findAll(@NotNull String userId);

    @NotNull
    List<TaskDTO> findAll(@NotNull String userId, @Nullable TaskSort sort);

    @NotNull
    List<TaskDTO> findAllByProjectId(@Nullable String userId, @Nullable String projectId);

    long getSize();

    long getSize(@NotNull String userId);

    boolean existsById(@Nullable final String userId, @Nullable final String id);

    void removeOneById(@Nullable String userId, @Nullable String id);

    void removeOneByIndex(@NotNull String userId, @NotNull Integer index);

    void removeAll();

    void removeAll(@NotNull List<TaskDTO> models);

    void removeAllByUserId(@Nullable String userId);

    void set(@NotNull Collection<TaskDTO> tasks);

    @NotNull
    TaskDTO updateById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    );

    @NotNull
    TaskDTO updateByIndex(
            @Nullable String userId,
            @Nullable Integer index,
            @Nullable String name,
            @Nullable String description
    );

}
