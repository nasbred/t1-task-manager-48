package ru.t1.kharitonova.tm.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kharitonova.tm.api.repository.model.ITaskRepository;
import ru.t1.kharitonova.tm.api.repository.model.IUserRepository;
import ru.t1.kharitonova.tm.api.service.IConnectionService;
import ru.t1.kharitonova.tm.api.service.model.ITaskService;
import ru.t1.kharitonova.tm.enumerated.Status;
import ru.t1.kharitonova.tm.enumerated.TaskSort;
import ru.t1.kharitonova.tm.exception.entity.ModelNotFoundException;
import ru.t1.kharitonova.tm.exception.entity.ProjectNotFoundException;
import ru.t1.kharitonova.tm.exception.entity.TaskNotFoundException;
import ru.t1.kharitonova.tm.exception.field.*;
import ru.t1.kharitonova.tm.exception.user.AccessDeniedException;
import ru.t1.kharitonova.tm.exception.user.UserNotFoundException;
import ru.t1.kharitonova.tm.model.Task;
import ru.t1.kharitonova.tm.model.User;
import ru.t1.kharitonova.tm.repository.model.TaskRepository;
import ru.t1.kharitonova.tm.repository.model.UserRepository;

import javax.persistence.EntityManager;
import java.util.List;

public final class TaskService extends AbstractUserOwnedService<Task, ITaskRepository>
        implements ITaskService {

    public TaskService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @NotNull
    @Override
    protected ITaskRepository getRepository(@NotNull final EntityManager entityManager) {
        return new TaskRepository(entityManager);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null) throw new DescriptionEmptyException();

        @NotNull Task task = new Task();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
        @Nullable final User user = userRepository.findOneById(userId);
        if (user == null) throw new UserNotFoundException();
        task.setName(name);
        task.setDescription(description);
        task.setUser(user);

        try {
            @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task changeTaskStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        @NotNull final EntityManager entityManager = getEntityManager();
        @Nullable Task task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        try {
            @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.changeStatusById(id, status);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task changeTaskStatusByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final Status status
    ) {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskRepository repository = getRepository(entityManager);

        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0 || index > repository.getSizeForUser(userId))
            throw new IndexIncorrectException();
        if (status == null) throw new StatusIncorrectException();

        @Nullable Task project = findOneByIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        try {
            entityManager.getTransaction().begin();
            repository.update(project);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return project;
    }

    @Override
    @SneakyThrows
    public long getSize() {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskRepository repository = getRepository(entityManager);
        try {
            return repository.getSize();
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public long getSize(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskRepository repository = getRepository(entityManager);
        try {
            return repository.getSizeForUser(userId);
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskRepository repository = getRepository(entityManager);
        try {
            return repository.existsById(id);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task findOneById(@Nullable final String user_id, @Nullable final String id) {
        if (user_id == null || user_id.isEmpty()) throw new IdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ITaskRepository repository = getRepository(entityManager);
            return repository.findOneByIdAndUserId(user_id, id);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task findOneByIndex(@Nullable final String userId, @Nullable final Integer index) {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskRepository repository = getRepository(entityManager);

        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0 || index > repository.getSizeForUser(userId))
            throw new IndexIncorrectException();
        try {
            return repository.findOneByIndexByUserId(userId, index);
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Task> findAll() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ITaskRepository repository = getRepository(entityManager);
            return repository.findAll();
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Task> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ITaskRepository repository = getRepository(entityManager);
            return repository.findAllByUserId(userId);
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Task> findAll(
            @Nullable final String userId,
            @Nullable final TaskSort sort
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (sort == null) return findAll(userId);
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ITaskRepository repository = getRepository(entityManager);
            return repository.findAllByUserIdWithSort(userId, sort.toString());
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public List<Task> findAllByProjectId(
            @Nullable final String userId,
            @Nullable final String projectId
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null) throw new ProjectIdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ITaskRepository repository = getRepository(entityManager);
            return repository.findAllByProjectId(userId, projectId);
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ITaskRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.removeOneById(id);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeOneByIndex(@Nullable final String userId, @Nullable final Integer index) {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskRepository repository = getRepository(entityManager);
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0 || index >= repository.getSizeForUser(userId))
            throw new IndexIncorrectException();
        try {
            entityManager.getTransaction().begin();
            repository.removeOneByIndexForUser(userId, index);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeAll() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ITaskRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.removeAll();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeAll(@Nullable final List<Task> models) {
        if (models == null) throw new ModelNotFoundException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ITaskRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.removeAllByList(models);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeAllByUserId(@Nullable final String userId) {
        if (userId == null) throw new UserIdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ITaskRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.removeAllByUserId(userId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();

        @Nullable final Task task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);

        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ITaskRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.update(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task updateByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskRepository repository = getRepository(entityManager);

        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (index == null || index < 0 || index >= repository.getSizeForUser(userId))
            throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @Nullable final Task task = findOneByIndex(userId, index);
        if (task == null) throw new ProjectNotFoundException();
        task.setName(name);
        task.setDescription(description);
        try {
            entityManager.getTransaction().begin();
            repository.update(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return task;
    }

}
