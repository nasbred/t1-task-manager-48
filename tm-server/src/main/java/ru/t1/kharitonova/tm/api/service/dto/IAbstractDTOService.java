package ru.t1.kharitonova.tm.api.service.dto;

import ru.t1.kharitonova.tm.api.repository.dto.IAbstractDTORepository;
import ru.t1.kharitonova.tm.dto.model.AbstractModelDTO;

public interface IAbstractDTOService<M extends AbstractModelDTO> extends IAbstractDTORepository<M> {
}
